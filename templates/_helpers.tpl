{{/*
是否开启online
*/}}
{{- define "app.hasOnline" -}}
  {{- if .Values.app.health.online -}}
    {{- printf "true" -}}
  {{- else -}}
    {{- printf "false" -}}
  {{- end -}}
{{- end -}}

{{/*
是否开启offline
*/}}
{{- define "app.hasOffline" -}}
  {{- if .Values.app.health.offline -}}
    {{- printf "true" -}}
  {{- else -}}
    {{- printf "false" -}}
  {{- end -}}
{{- end -}}

{{/*
是否开启lifecycle(即开启了online或者offline)
*/}}
{{- define "app.hasLifecycle" -}}
  {{- if or (eq (include "app.hasOnline" .) "true") (eq (include "app.hasOffline" .) "true") -}}
    {{- printf "true" -}}
  {{- else -}}
    {{- printf "false" -}}
  {{- end -}}
{{- end -}}

{{/*
是否开启check检查
*/}}
{{- define "app.hasCheck" -}}
  {{- if .Values.app.health.check -}}
    {{- printf "true" -}}
  {{- else -}}
    {{- printf "false" -}}
  {{- end -}}
{{- end -}}

{{/*
是否开启status检查
*/}}
{{- define "app.hasStatus" -}}
  {{- if .Values.app.health.status -}}
    {{- printf "true" -}}
  {{- else -}}
    {{- printf "false" -}}
  {{- end -}}
{{- end -}}

{{/*
定义podAnnotations:
1. user-restart-time
2. pre环境定义对应的ippool，Ref: http://kubernetes.netease.com/%E5%AE%B9%E5%99%A8%E7%BD%91%E7%BB%9C/scud/%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E.html
*/}}
{{- define "podAnnotations" -}}
cloudnative.music.netease.com/http-probe.sh: |
{{ .Files.Get "files/http-probe.sh" | indent 2}}
{{- if eq (include "app.hasOnline" .) "true" }}
cloudnative.music.netease.com/online.sh: |
{{ tpl (.Files.Get "files/online.sh") . | indent 2}}
cloudnative.music.netease.com/online-once.sh: |
{{ tpl (.Files.Get "files/once/online.sh") . | indent 2}}
{{- end }}
{{- if eq (include "app.hasOffline" .) "true" }}
cloudnative.music.netease.com/offline.sh: |
{{ tpl (.Files.Get "files/offline.sh") . | indent 2}}
cloudnative.music.netease.com/offline-once.sh: |
{{ tpl (.Files.Get "files/once/offline.sh") . | indent 2}}
{{- end }}
{{- if eq (include "app.hasStatus" .) "true" }}
cloudnative.music.netease.com/status.sh: |
{{ tpl (.Files.Get "files/status.sh") . | indent 2}}
{{- end }}
{{- if eq (include "app.hasCheck" .) "true" }}
cloudnative.music.netease.com/startup.sh: |
{{ tpl (.Files.Get "files/startup.sh") . | indent 2}}
{{- end }}
{{- if .Values.restartTime }}
cloudnative.music.netease.com/user-restart-time: {{ .Values.restartTime }}
{{- end }}
{{- if eq .Values.env.environment "pre" }}
network.netease.com/kubernetes.ippool.name: pre
{{- end }}
{{- if .Values.git }}
{{- if .Values.git.url }}
cloudnative.music.netease.com/git-url: {{ .Values.git.url }}
{{- end }}
{{- if .Values.git.branch }}
cloudnative.music.netease.com/git-branch: {{ .Values.git.branch | quote }}
{{- end }}
{{- if .Values.git.commitID }}
cloudnative.music.netease.com/git-commit: {{ .Values.git.commitID }}
{{- end }}
{{- end }}
{{- end }}

{{/*
定义jvmExtra
*/}}
{{- define "jvmExtra" -}}
  {{- $jvmExtra := .Values.app.params.jvmExtra | default "" -}}
  {{- $jvmExtraInTag := .Values.tags.jvmExtra | default .Values.tags.jvm | default "" -}}
  {{- $jvmMerge := cat $jvmExtra $jvmExtraInTag -}}
  {{- if not (contains "-Dcom.cmdb.appname" $jvmMerge) }}
    {{- $jvmMerge = cat $jvmMerge (printf "-Dcom.cmdb.appname=%v" .Values.horizon.application) }}
  {{- end }}
  {{- if not (contains "-Dcom.cmdb.clustername" $jvmMerge) }}
    {{- $jvmMerge = cat $jvmMerge (printf "-Dcom.cmdb.clustername=%v" .Values.horizon.cluster) }}
  {{- end }}
  {{- if not (contains "-Denv" $jvmMerge) }}
    {{- $jvmMerge = cat $jvmMerge (printf "-Denv=%v" .Values.env.environment) }}
  {{- end }}
  {{- printf "%v" (trim $jvmMerge) }}
{{- end }}

{{/*
定义nodeSelectorKey
NOTE: 用于性能测试的应用集群，需要被部署在独立的机器上
*/}}
{{- define "nodeSelectorKey" -}}
  {{- if eq .Values.env.environment "perf" }}
    {{- printf "perf/compute" -}}
  {{- else }}
    {{- printf "cicd/compute" -}}
  {{- end }}
{{- end }}

{{/*
定义cpuRequest，用于超售
超售规则：
1. pre和online环境，P0以外应用2.5倍超售
2. 其他环境，全部4倍超售
*/}}
{{- define "cpuRequest" -}}
{{- $resource := .Values.app.spec.resource -}}
{{- $cpu := index (index (fromJson (tpl (.Values.resources | toJson) .)) $resource) "cpu" -}}
{{- if or (eq .Values.env.environment "online") (eq .Values.env.environment "pre") }}
  {{- if not (eq .Values.horizon.priority "P0") }}
    {{- $cpu = div (mul $cpu 10) .Values.oversale.cpu.online }}
  {{- end }}
{{- else }}
  {{- $cpu = div (mul $cpu 10) .Values.oversale.cpu.others }}
{{- end }}
{{- printf "%v" $cpu }}
{{- end }}

{{/*
定义cpuLimit
*/}}
{{- define "cpuLimit" -}}
{{- $resource := .Values.app.spec.resource -}}
{{- $cpu := index (index (fromJson (tpl (.Values.resources | toJson) .)) $resource) "cpu" -}}
{{- printf "%v" $cpu }}
{{- end }}

{{/*
定义期望的cpu使用情况
*/}}
{{- define "expectCPUUsageValue" -}}
{{- $cpuLimit := include "cpuLimit" . -}}
{{- $expect := div (mul $cpuLimit .Values.app.spec.cpuUtilization) 100 }}
{{- printf "%vm" $expect }}
{{- end }}

{{/*
定义memoryRequest，用于超售
超售规则：
1. 除pre和online的其他环境，memoryRequest = xms + 256
*/}}
{{- define "memoryRequest" -}}
{{- $resource := .Values.app.spec.resource -}}
{{- $memory := index (index (fromJson (tpl (.Values.resources | toJson) .)) $resource) "memory" -}}
{{- $jvmExtra := include "jvmExtra" . -}}
{{- $memoryDelta := .Values.oversale.memory.delta -}}
{{- if not (or (eq .Values.env.environment "online") (eq .Values.env.environment "pre")) }}
  {{- if regexFind "-Xms(.+?)m" $jvmExtra }}
    {{- $memory = regexReplaceAll "-Xms(.+?)m" (regexFind "-Xms(.+?)m" $jvmExtra) "${1}" | add $memoryDelta }}
  {{- else if .Values.app.params.xms }}
    {{- $memory = .Values.app.params.xms | add $memoryDelta }}
  {{- end }}
{{- end }}
{{- printf "%v" $memory }}
{{- end }}

{{/*
定义memoryLimit
*/}}
{{- define "memoryLimit" -}}
{{- $resource := .Values.app.spec.resource -}}
{{- $memory := index (index (fromJson (tpl (.Values.resources | toJson) .)) $resource) "memory" -}}
{{- printf "%v" $memory }}
{{- end }}

{{- define "resources" -}}
requests:
  ephemeral-storage: "128Mi"
  cpu: {{ include "cpuRequest" . }}m
  memory: {{ min (include "memoryLimit" .) (include "memoryRequest" .) }}Mi
limits:
  ephemeral-storage: "60Gi"
  cpu: {{ include "cpuLimit" . }}m
  memory: {{ include "memoryLimit" . }}Mi
{{- end }}



{{- define "podLabels" -}}
cloudnative.music.netease.com/application: {{ .Values.horizon.application }}
cloudnative.music.netease.com/cluster: {{ .Values.horizon.cluster }}
cloudnative.music.netease.com/environment: {{ .Values.env.environment }}
{{- end }}


{{/*
定义 memcached resource
*/}}
{{- define "memcachedMemory" -}}
{{- $size := .Values.memcached.size -}}
{{- $mmemory := index (index .Values.memcached.memcachedResources $size) "memory" -}}
{{- printf "%v" $mmemory}}
{{- end }}

{{- define "memcachedCpu" -}}
{{- $size := .Values.memcached.size -}}
{{- $mcpu := index (index .Values.memcached.memcachedResources $size) "cpu" -}}
{{- printf "%v" $mcpu}}
{{- end }}

{{- define "memcachedResource" -}}
requests:
  cpu: {{ include "memcachedCpu" . }}m
  memory: {{ include "memcachedMemory" . }}Mi
limits:
  cpu: {{ include "memcachedCpu" . }}m
  memory: {{ include "memcachedMemory" . }}Mi
{{- end }}

{{/*
定义 memcached db size
*/}}
{{- define "memcachedDBsize" -}}
{{- $mdbsize := include "memcachedMemory" . -}}
{{- $mdbsize = div (mul $mdbsize 3) 4 }}
{{- printf "%v" $mdbsize}}
{{- end }}


{{- define "canarySteps" -}}

{{- if eq .Values.app.spec.autoscale "none" -}}
{{- if eq (int .Values.app.spec.replicas) 0 }}
  {{- printf "[]" -}}
{{- else }}
  {{- include "getCanarySteps" . }}
{{- end }}

{{- else }}

{{- if eq (int .Values.app.spec.minReplicas) 0 }}
  {{- printf "[]" -}}
{{- else }}
  {{- include "getCanarySteps" . }}
{{- end }}
{{- end }}
{{- end }}


{{- define "getCanarySteps" -}}
{{- $stepsTotal := 0 -}}
{{- if eq .Values.app.spec.autoscale "none" -}}
{{- $stepsTotal = int (min .Values.app.spec.replicas .Values.app.strategy.stepsTotal) -}}
{{- else }}
{{- $stepsTotal = int (min .Values.app.spec.minReplicas .Values.app.strategy.stepsTotal) -}}
{{- end }}
{{- $rate := div 100 $stepsTotal -}}
{{- if gt $stepsTotal 1 }}
  {{- $rate = div 100 (sub $stepsTotal 1) -}}
{{- end }}
{{- $pauseType := .Values.app.strategy.pauseType -}}
{{- $steps := "[" -}}
  {{- range $index := until $stepsTotal }}
    {{- $stepRate := mul $index $rate -}}
    {{- if eq (add $index 1) $stepsTotal -}}
      {{- $stepRate = "100" -}}
    {{- else if and (eq $index 0) (not (eq $stepsTotal 1)) -}}
      {{- $stepRate = "1" -}}
    {{- end -}}
    {{- $steps = cat $steps (printf "\"setWeight\": %v ," $stepRate) -}}
    {{- if and (eq $index 0) (not (eq $stepsTotal 1)) (not (eq $pauseType "none")) -}}
      {{- $steps = cat $steps "\"pause\": {}" "," }}
    {{- else if and (not (eq (add $index 1) $stepsTotal)) (eq $pauseType "all") -}}
      {{- $steps = cat $steps "\"pause\": {}" "," }}
    {{- end -}}
  {{- end -}}
{{- $steps = trimSuffix "," $steps -}}
{{- $steps = cat $steps "]" -}}
{{- printf "%v" $steps -}}
{{- end }}
