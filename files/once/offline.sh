. "$(cd "$(dirname "$0")";pwd)/http-probe.sh"
offline(){
  local URL="http://localhost:{{ .Values.app.health.port }}{{ .Values.app.health.offline }}"
  local RETRY=1
  local SLEEP=10
  local TIMEOUT=3
  local APPHOME={{ .Values.app.home }}
  local SlEEP_SECONDS_WHEN_SUCCEED=0
  http_probe "offline(下线)" "$URL" "$RETRY" "$SLEEP" "$TIMEOUT" "$SlEEP_SECONDS_WHEN_SUCCEED" "$APPHOME"
} && offline