HTTP_CODE="$(curl -ksSL -w '%{http_code}' -m 3 -o /dev/null "http://localhost:{{ .Values.app.health.port }}{{ .Values.app.health.check }}")"
(("$HTTP_CODE"<200)) || (("$HTTP_CODE">=400)) && exit 1
exit 0