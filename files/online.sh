. "$(cd "$(dirname "$0")";pwd)/http-probe.sh"
online(){
  local URL="http://localhost:{{ .Values.app.health.port }}{{ .Values.app.health.online }}"
  local RETRY={{ max (div .Values.app.health.expectedStartTime .Values.health.online.sleep) 1 }}
  local SLEEP={{ .Values.health.online.sleep | default 10 }}
  local TIMEOUT={{ .Values.health.online.timeout | default 30 }}
  local APPHOME={{ .Values.app.home }}
  local SlEEP_SECONDS_WHEN_SUCCEED=0
  http_probe "online(上线)" "$URL" "$RETRY" "$SLEEP" "$TIMEOUT" "$SlEEP_SECONDS_WHEN_SUCCEED" "$APPHOME"
}
{{- if .Values.app.health.check }}
check(){
  local URL="http://localhost:{{ .Values.app.health.port }}{{ .Values.app.health.check }}"
  local RETRY={{ max (div .Values.app.health.expectedStartTime .Values.health.online.sleep) 1 }}
  local SLEEP={{ .Values.health.online.sleep | default 10 }}
  local TIMEOUT={{ .Values.health.online.timeout | default 30 }}
  local APPHOME={{ .Values.app.home }}
  local SlEEP_SECONDS_WHEN_SUCCEED=0
  http_probe "check(存活状态)" "$URL" "$RETRY" "$SLEEP" "$TIMEOUT" "$SlEEP_SECONDS_WHEN_SUCCEED" "$APPHOME"
}
{{- end }}

{{- if eq .Values.memcached.enabled  true }}
checkmemcache(){
  for i in {1..{{ .Values.app.checkTime }}}
  do
    exist=$(ss -l | grep "*:{{ .Values.memcached.memcached.listenPort }}" | grep "tcp" | wc -l)
    if [ $exist -eq 1 ]
    then
      echo "memcached check ok"
      return 0
    else
      echo "memcached check failed, wait and retry"
      sleep {{ .Values.app.checkPeriod }}
    fi
  done
  echo "memcached startup check error"
  return 1
}
{{- end }}

{{- if and  .Values.app.health.check (eq .Values.memcached.enabled  true) }}
checkmemcache &&  check  && online
{{- else if .Values.app.health.check }}
check && online
{{- else if eq .Values.memcached.enabled  true }}
checkmemcache && online
{{- else}}
online
{{- end }}
