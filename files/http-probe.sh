http_probe(){
  local FUNC="$1" URL="$2" RETRY="$3" SLEEP="$4" \
        TIMEOUT="$5" SLEEP_WHEN_SUCCEED="$6"  BASEDIR="$7" \
        PROBE_RESULT_DIR="${PROBE_RESULT_DIR:-".probe-result"}"
  local RESP_BODY_FILE="$BASEDIR/$PROBE_RESULT_DIR/$FUNC.res" ERROR_FILE="$BASEDIR/$PROBE_RESULT_DIR/$FUNC.err" && \
       { [[ ! -d "$BASEDIR/$PROBE_RESULT_DIR" ]] && mkdir -p "$BASEDIR/$PROBE_RESULT_DIR"; }
  local CHECK_STATUS BODY ERROR
  for ((i=1;i<="$RETRY";i++)); do
    rm -f "$RESP_BODY_FILE" "$ERROR_FILE"
    CHECK_STATUS="$(curl -ksSL -w '%{http_code}' -m "$TIMEOUT" "$URL" -o "$RESP_BODY_FILE" 2>"$ERROR_FILE")"
    [[ "$CHECK_STATUS" == 20* ]] && echo "$FUNC ok!" && {
      if [[ ! -z "$SLEEP_WHEN_SUCCEED" ]]; then
        echo "sleep "$SLEEP_WHEN_SUCCEED"s before "$FUNC" return ..." && sleep "$SLEEP_WHEN_SUCCEED" 
      fi
      return 0
    } 
    (("$i"<"$RETRY")) && sleep "$SLEEP"
  done
  echo "$FUNC failed after $RETRY times retry ..."
  [[ -f "$RESP_BODY_FILE" ]] && {
    BODY="$(cat "$RESP_BODY_FILE" | uniq)"; [[ ! -z "$BODY" ]] && echo "http code is: $CHECK_STATUS; response body is: $BODY"
  }
  [[ -f "$ERROR_FILE" ]] && {
    ERROR="$(cat "$ERROR_FILE" | uniq)"; [[ ! -z "$ERROR" ]] && echo "error is: $ERROR"
  }
  return 1
}