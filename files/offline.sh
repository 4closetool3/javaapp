. "$(cd "$(dirname "$0")";pwd)/http-probe.sh"
offline(){
  local URL="http://localhost:{{ .Values.app.health.port }}{{ .Values.app.health.offline }}"
  local RETRY={{ .Values.health.offline.retry | default 2 }}
  local SLEEP={{ .Values.health.offline.sleep | default 10 }}
  local TIMEOUT={{ .Values.health.offline.timeout | default 5 }}
  local APPHOME={{ .Values.app.home }}
  local SlEEP_SECONDS_WHEN_SUCCEED=10
  http_probe "offline(下线)" "$URL" "$RETRY" "$SLEEP" "$TIMEOUT" "$SlEEP_SECONDS_WHEN_SUCCEED" "$APPHOME"
} && offline