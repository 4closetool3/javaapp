{{- if .Values.app.health.check }}
HTTP_CODE="$(curl -ksSL -w '%{http_code}' -m 3 -o /dev/null "http://localhost:{{ .Values.app.health.port }}{{ .Values.app.health.check }}")"
(("$HTTP_CODE"<200)) || (("$HTTP_CODE">=400)) && exit 1
{{- end }}
is_json(){
  local INPUT="$1"
  [[ ! -z "$(jq -c "objects"<<<"$INPUT" 2>/dev/null)" ]] && return 0 || return 1
}
IFS='#' read -r RESPONSE_BODY HTTP_CODE < <(curl -m 3 -s -w '#%{http_code}' 'http://localhost:{{ .Values.app.health.port }}{{ .Values.app.health.status }}')
(("$HTTP_CODE"<200)) || (("$HTTP_CODE">=400)) && exit 1
[[ -z "$RESPONSE_BODY" ]] && exit 0
RESPONSE_BODY="${RESPONSE_BODY,,}"
if [[ "$RESPONSE_BODY" != "200" ]] && [[ "$RESPONSE_BODY" != "alive" ]] && [[ "$RESPONSE_BODY" != "ok" ]] && [[ "$RESPONSE_BODY" != "online" ]]; then
  if ! is_json "$RESPONSE_BODY"; then
    exit 1
  fi
  CODE="$(jq -r '.code'<<<"$RESPONSE_BODY")"
  [[ "$CODE" == "200" ]] || exit 1
fi